import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

xdescribe('HeaderComponent', () => {
  let utObjects;
  let component;

  beforeEach(() => {
    utObjects.authenticationService = jasmine.createSpyObj('authenticationService', 
      ['currentUser', 'currentUserValue']);
      utObjects.route = jasmine.createSpyObj('route', ['navigate']);
   component = new HeaderComponent(
      utObjects.authenticationService,
      utObjects.route
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
