import { Module, NestModule, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ReservesModule } from './reserves/reserves.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FrontendMiddleware } from './frontend.middleware';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "mysql",
      host: "remotemysql.com",
      port: 3306,
      username: "nWMcb2Fz1X",
      password: "3B2yG3f5J4",
      database: "nWMcb2Fz1X",
      entities: [__dirname + "/**/entities/*.entity{.ts,.js}"],
      synchronize: true
    }),
    AuthModule,
    UsersModule,
    ReservesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void | MiddlewareConsumer {
    consumer.apply(FrontendMiddleware).forRoutes(
      {
        path: '/**', // For all routes
        method: RequestMethod.ALL, // For all methods
      },
    );
  }
 
}
